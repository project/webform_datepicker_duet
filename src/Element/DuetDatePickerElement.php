<?php

namespace Drupal\webform_datepicker_duet\Element;

use Drupal\Core\Datetime\Element\DateElementBase;
use Drupal\Core\Render\Element;
use Drupal\Core\Form\FormStateInterface;


/**
 * Provides a form element for a Duet datepicker element.
 *
 * @FormElement ("webform_datepicker_duet")
 */
class DuetDatePickerElement extends DateElementBase {
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#size' => 60,
      '#process' => [
        [$class, 'processDuetDatePickerElement'],
        [$class, 'processAjaxForm'],
      ],
      '#element_validate' => [
        [$class, 'validateDuetDatePickerElement'],
      ],
      '#pre_render' => [
        [$class, 'preRenderDuetDatePickerElement'],
      ],
      '#theme' => 'input__webform_datepicker_duet',
      '#theme_wrappers' => ['form_element'],
    ];

  }

  /**
   * Processes a 'webform_datepicker_duet' element.
   */
  public static function processDuetDatePickerElement(&$element, FormStateInterface $form_state, &$complete_form) {
    $date_min = $element['#date_date_min'];
    $date_max = $element['#date_date_max'];
    return $element;
  }

  /**
   * Webform element validation handler for #type 'webform_datepicker_duet'.
   */
  public static function validateDuetDatePickerElement(&$element, FormStateInterface $form_state, &$complete_form) {
    // Here you can add custom validation logic.
  }

  /**
   * Prepares a #type 'datepicker_duet' render element for theme_element().
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   *   Properties used: #title, #value, #description, #size, #maxlength,
   *   #placeholder, #required, #attributes.
   *
   * @return array
   *   The $element with prepared variables ready for theme_element().
   */
  public static function preRenderDuetDatePickerElement(array $element) {
    $element['#attributes']['type'] = 'text';
    Element::setAttributes($element, ['id', 'name', 'value', 'size', 'maxlength', 'placeholder']);
    static::setAttributes($element, ['form-text', 'webform-datepicker-duet-element']);
    return $element;
  }

}
