<?php

namespace Drupal\webform_datepicker_duet\Plugin\WebformElement;

use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\Date;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Provides a 'webform_datepicker_duet' element.
 *
 * @WebformElement(
 *   id = "webform_datepicker_duet",
 *   label = @Translation("Duet datepicker"),
 *   description = @Translation("Provides a Duet datepicker webform element."),
 *   category = @Translation("Custom elements"),
 * )
 */
class DuetDatePickerElement extends Date {
  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    // Hide the Use date picker radios button because it's not needed.
    $form['date']['datepicker']['#access'] = FALSE;

    $form['date']['datepicker_button'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show date picker button'),
      '#description' => $this->t('If checked, date picker will include a calendar button'),
      '#return_value' => TRUE,
    ];
    $date_format = DateFormat::load('html_date')->getPattern();
    $form['date']['date_date_format'] = [
      '#type' => 'webform_select_other',
      '#title' => $this->t('Date format'),
      '#options' => [
        $date_format => $this->t('HTML date - @format (@date)', ['@format' => $date_format, '@date' => static::formatDate($date_format)]),
        'l, F j, Y' => $this->t('Long date - @format (@date)', ['@format' => 'l, F j, Y', '@date' => static::formatDate('l, F j, Y')]),
        'D, m/d/Y' => $this->t('Medium date - @format (@date)', ['@format' => 'D, m/d/Y', '@date' => static::formatDate('D, m/d/Y')]),
        'm/d/Y' => $this->t('Short date - @format (@date)', ['@format' => 'm/d/Y', '@date' => static::formatDate('m/d/Y')]),
      ],
      '#description' => $this->t("Date format is only applicable for browsers that do not have support for the HTML5 date element. Browsers that support the HTML5 date element will display the date using the user's preferred format."),
      '#other__option_label' => $this->t('Custom…'),
      '#other__placeholder' => $this->t('Custom date format…'),
      '#other__description' => $this->t('Enter date format using <a href="http://php.net/manual/en/function.date.php">Date Input Format</a>.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function prepare(array &$element, WebformSubmissionInterface $webform_submission = NULL) {
    $element['#attached']['library'][] = 'webform_datepicker_duet/duet';
    parent::prepare($element, $webform_submission);
  }

}

