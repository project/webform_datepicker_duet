CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation


INTRODUCTION
------------

The Webform datepicker Duet module lets you integrate datepicker component from Duet Design System into Drupal Webform.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/webform_datepicker_duet
* For a full description of the datepicker component, visit the project page:
  https://github.com/duetds/date-picker

REQUIREMENTS
------------

No special requirements.

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module.
  See: https://www.drupal.org/docs/8/extending-drupal-8/installing-modules
  for further information.


MAINTAINERS
-----------

Current maintainers:
* Robert Ngo (Robert Ngo) - https://www.drupal.org/u/robert-ngo
